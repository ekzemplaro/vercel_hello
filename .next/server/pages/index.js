"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./locales/en.js":
/*!***********************!*\
  !*** ./locales/en.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n// en.js\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n    SUBTITLE: \"Visualize your customer happiness.\",\n    TO_DASHBOARD: \"Go to dashboard\",\n    DATE: \"May/17/2022 PM 17:30\",\n    GREETING: \"Shimotsuke (\\u4E0B\\u91CE\\u5E02, Shimotsuke-shi) is a city located in Tochigi Prefecture, Japan. As of 1 August 2020, the city had an estimated population of 60,274 in 24,654 households, and a population density of 810 persons per km\\xb2. The total area of the city is 74.59 square kilometres (28.80 sq mi).\"\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9sb2NhbGVzL2VuLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFBQSxRQUFRO0FBQ1IsaUVBQWU7SUFDYkEsUUFBUSxFQUFFLG9DQUFvQztJQUM5Q0MsWUFBWSxFQUFFLGlCQUFpQjtJQUMvQkMsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QkMsUUFBUSxFQUFFLG9UQUFrUztDQUU3UyIsInNvdXJjZXMiOlsid2VicGFjazovL3Byb2owMi8uL2xvY2FsZXMvZW4uanM/MjlhNCJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBlbi5qc1xuZXhwb3J0IGRlZmF1bHQge1xuICBTVUJUSVRMRTogXCJWaXN1YWxpemUgeW91ciBjdXN0b21lciBoYXBwaW5lc3MuXCIsXG4gIFRPX0RBU0hCT0FSRDogXCJHbyB0byBkYXNoYm9hcmRcIixcbiAgREFURTogXCJNYXkvMTcvMjAyMiBQTSAxNzozMFwiLFxuICBHUkVFVElORzogXCJTaGltb3RzdWtlICjkuIvph47luIIsIFNoaW1vdHN1a2Utc2hpKSBpcyBhIGNpdHkgbG9jYXRlZCBpbiBUb2NoaWdpIFByZWZlY3R1cmUsIEphcGFuLiBBcyBvZiAxIEF1Z3VzdCAyMDIwLCB0aGUgY2l0eSBoYWQgYW4gZXN0aW1hdGVkIHBvcHVsYXRpb24gb2YgNjAsMjc0IGluIDI0LDY1NCBob3VzZWhvbGRzLCBhbmQgYSBwb3B1bGF0aW9uIGRlbnNpdHkgb2YgODEwIHBlcnNvbnMgcGVyIGttwrIuIFRoZSB0b3RhbCBhcmVhIG9mIHRoZSBjaXR5IGlzIDc0LjU5IHNxdWFyZSBraWxvbWV0cmVzICgyOC44MCBzcSBtaSkuXCIsXG4gIC8vIC4uLlxufVxuIl0sIm5hbWVzIjpbIlNVQlRJVExFIiwiVE9fREFTSEJPQVJEIiwiREFURSIsIkdSRUVUSU5HIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./locales/en.js\n");

/***/ }),

/***/ "./locales/ja.js":
/*!***********************!*\
  !*** ./locales/ja.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({\n    SUBTITLE: \"\\u300C\\u6E80\\u8DB3\\u300D\\u3092\\u53EF\\u8996\\u5316\\u3057\\u3088\\u3046\",\n    TO_DASHBOARD: \"\\u30C0\\u30C3\\u30B7\\u30E5\\u30DC\\u30FC\\u30C9\\u3078\",\n    DATE: \"2022\\u5E745\\u670817\\u65E5 \\u5348\\u5F8C5\\u664230\\u5206\",\n    GREETING: \"\\u4E0B\\u91CE\\u5E02\\uFF08\\u3057\\u3082\\u3064\\u3051\\u3057\\uFF09\\u306F\\u3001\\u6803\\u6728\\u770C\\u5357\\u90E8\\u306B\\u4F4D\\u7F6E\\u3059\\u308B\\u4EBA\\u53E3\\u7D046\\u4E07\\u4EBA\\u306E\\u5E02\\u3002\\u5B87\\u90FD\\u5BAE\\u5E02\\u3078\\u306E\\u901A\\u52E4\\u7387\\u306F13.3%\\u3001\\u5C0F\\u5C71\\u5E02\\u3078\\u306E\\u901A\\u52E4\\u7387\\u306F11.1%\\uFF08\\u3044\\u305A\\u308C\\u3082\\u5E73\\u621022\\u5E74\\u56FD\\u52E2\\u8ABF\\u67FB\\uFF09\\u3002\\u95A2\\u6771\\u5927\\u90FD\\u5E02\\u570F\\u306B\\u5C5E\\u3059\\u308B\\u3002\"\n});\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9sb2NhbGVzL2phLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7QUFBQSxpRUFBZTtJQUNiQSxRQUFRLEVBQUUsb0VBQWE7SUFDREMsWUFBVixFQUFFLGtEQUFVO0lBQ1JDLElBQVosRUFBRSx1REFBb0I7SUFDWkMsUUFBTixFQUFFLGlkQUEwRjtDQUVyRyIsInNvdXJjZXMiOlsid2VicGFjazovL3Byb2owMi8uL2xvY2FsZXMvamEuanM/MmFkYiJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCB7XG4gIFNVQlRJVExFOiBcIuOAjOa6gOi2s+OAjeOCkuWPr+imluWMluOBl+OCiOOBhlwiLFxuICBUT19EQVNIQk9BUkQ6IFwi44OA44OD44K344Ol44Oc44O844OJ44G4XCIsXG4gIERBVEU6IFwiMjAyMuW5tDXmnIgxN+aXpSDljYjlvow15pmCMzDliIZcIixcbiAgR1JFRVRJTkc6IFwi5LiL6YeO5biC77yI44GX44KC44Gk44GR44GX77yJ44Gv44CB5qCD5pyo55yM5Y2X6YOo44Gr5L2N572u44GZ44KL5Lq65Y+j57SENuS4h+S6uuOBruW4guOAguWuh+mDveWuruW4guOBuOOBrumAmuWLpOeOh+OBrzEzLjMl44CB5bCP5bGx5biC44G444Gu6YCa5Yuk546H44GvMTEuMSXvvIjjgYTjgZrjgozjgoLlubPmiJAyMuW5tOWbveWLouiqv+afu++8ieOAgumWouadseWkp+mDveW4guWcj+OBq+WxnuOBmeOCi+OAglwiLFxuICAvLyAuLi5cbn1cbiJdLCJuYW1lcyI6WyJTVUJUSVRMRSIsIlRPX0RBU0hCT0FSRCIsIkRBVEUiLCJHUkVFVElORyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./locales/ja.js\n");

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ Home)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ \"next/router\");\n/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _locales_en__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../locales/en */ \"./locales/en.js\");\n/* harmony import */ var _locales_ja__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../locales/ja */ \"./locales/ja.js\");\n\n\n\n\nfunction Home() {\n    const { locale  } = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();\n    const t = locale === \"en\" ? _locales_en__WEBPACK_IMPORTED_MODULE_2__[\"default\"] : _locales_ja__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        children: [\n            \"...\",\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n                children: t.GREETING\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_hello/pages/index.js\",\n                lineNumber: 13,\n                columnNumber: 10\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h2\", {\n                children: t.SUBTITLE\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_hello/pages/index.js\",\n                lineNumber: 14,\n                columnNumber: 10\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"button\", {\n                children: t.TO_DASHBOARD\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_hello/pages/index.js\",\n                lineNumber: 15,\n                columnNumber: 10\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"p\", {\n                children: t.DATE\n            }, void 0, false, {\n                fileName: \"/home/uchida/html/gitlab/vercel_hello/pages/index.js\",\n                lineNumber: 16,\n                columnNumber: 10\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/home/uchida/html/gitlab/vercel_hello/pages/index.js\",\n        lineNumber: 11,\n        columnNumber: 7\n    }, this);\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5qcy5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFBd0M7QUFDVDtBQUNBO0FBRWhCLFNBQVNHLElBQUksR0FBRztJQUM1QixNQUFNLEVBQUVDLE1BQU0sR0FBRSxHQUFHSixzREFBUyxFQUFFO0lBRTlCLE1BQU1LLENBQUMsR0FBR0QsTUFBTSxLQUFLLElBQUksR0FBR0gsbURBQUUsR0FBR0MsbURBQUU7SUFFbkMscUJBQ0csOERBQUNJLEtBQUc7O1lBQUMsS0FFRjswQkFBQSw4REFBQ0MsR0FBQzswQkFBRUYsQ0FBQyxDQUFDRyxRQUFROzs7OztvQkFBSzswQkFDbkIsOERBQUNDLElBQUU7MEJBQUVKLENBQUMsQ0FBQ0ssUUFBUTs7Ozs7b0JBQU07MEJBQ3JCLDhEQUFDQyxRQUFNOzBCQUFFTixDQUFDLENBQUNPLFlBQVk7Ozs7O29CQUFVOzBCQUNqQyw4REFBQ0wsR0FBQzswQkFBRUYsQ0FBQyxDQUFDUSxJQUFJOzs7OztvQkFBSzs7Ozs7O1lBQ1osQ0FDUjtDQUNIIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcHJvajAyLy4vcGFnZXMvaW5kZXguanM/YmVlNyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcbmltcG9ydCBlbiBmcm9tIFwiLi4vbG9jYWxlcy9lblwiO1xuaW1wb3J0IGphIGZyb20gXCIuLi9sb2NhbGVzL2phXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhvbWUoKSB7XG4gICBjb25zdCB7IGxvY2FsZSB9ID0gdXNlUm91dGVyKClcblxuICAgY29uc3QgdCA9IGxvY2FsZSA9PT0gXCJlblwiID8gZW4gOiBqYTtcblxuICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgICAuLi5cbiAgICAgICAgIDxwPnt0LkdSRUVUSU5HfTwvcD5cbiAgICAgICAgIDxoMj57dC5TVUJUSVRMRX08L2gyPlxuICAgICAgICAgPGJ1dHRvbj57dC5UT19EQVNIQk9BUkR9PC9idXR0b24+XG4gICAgICAgICA8cD57dC5EQVRFfTwvcD5cbiAgICAgIDwvZGl2PlxuICAgKVxufVxuXG4iXSwibmFtZXMiOlsidXNlUm91dGVyIiwiZW4iLCJqYSIsIkhvbWUiLCJsb2NhbGUiLCJ0IiwiZGl2IiwicCIsIkdSRUVUSU5HIiwiaDIiLCJTVUJUSVRMRSIsImJ1dHRvbiIsIlRPX0RBU0hCT0FSRCIsIkRBVEUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/index.js\n");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.js"));
module.exports = __webpack_exports__;

})();