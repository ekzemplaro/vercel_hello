// en.js
export default {
  SUBTITLE: "Visualize your customer happiness.",
  TO_DASHBOARD: "Go to dashboard",
  DATE: "May/17/2022 PM 17:30",
  GREETING: "Shimotsuke (下野市, Shimotsuke-shi) is a city located in Tochigi Prefecture, Japan. As of 1 August 2020, the city had an estimated population of 60,274 in 24,654 households, and a population density of 810 persons per km². The total area of the city is 74.59 square kilometres (28.80 sq mi).",
  // ...
}
